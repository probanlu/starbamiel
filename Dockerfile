FROM python:3.8.5-slim
WORKDIR /app
RUN pip3 install discord.py==1.7.3
COPY . .

RUN addgroup --system starbamielg && adduser --system --ingroup starbamielg starbamiel
RUN chown starbamiel:starbamielg /app
USER starbamiel

CMD ["python3", "starboard.py"]
