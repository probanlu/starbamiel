### ------ START CONFIG ------ ###

# Add your list of emojis
emojis = [
    "✨",
    "🌟",
    "🎇",
    "⭐",
]

# Set the emoji count. This will be the sum of all emojis,
# so 2✨ and 3⭐ would be 5
count = 5

# Bot token must be set as environment variable BOT_TOKEN, it cannot be set here.

# Enter the ID of each guild and its starboard channel
starboard_channel_ids_by_guild_id = {
    275358751630819328: 389571709470113793, # C418
    432636666834845717: 479379482872905739, # c418 testing
    748742888174649475: 748742889508700203, # C418 Testing Server - #418
    748757789471408152: 748757789941301373, # PRIZ ;]'s server
}

# Enter the bot prefix
bot_prefix = "!starbamiel"

# Allow multiple reactions per user
allow_multi_reactions_per_user = False

### ------- END CONFIG ------- ###

# WARNING: Do not edit below this line unless
# you know *exactly* what you're doing.
# ----------------------------------------------- #

import asyncio
import os
import io
import discord
from discord.ext import commands
from discord.ext.commands import Bot
import embedify
import database as dbman
import traceback
import datetime
import re

if 'REACT_THRESHOLD' in os.environ:
    count = int(os.environ['REACT_THRESHOLD'])

TOKEN = os.environ['BOT_TOKEN']

bot = commands.Bot(
    command_prefix = bot_prefix
)

# Only used when the reaction emoji is completely missing
class FalseReaction:
    def __init__(self, emoji, message):
        self.count = 0
        self.me = False
        self.emoji = emoji
        self.custom_emoji = emoji.is_custom_emoji()
        self.message = message

# Get the reaction and the user from the payload
async def from_rct_payload(payload):
    chn = await bot.fetch_channel(payload.channel_id)
    msg = await chn.fetch_message(payload.message_id)
    for reaction in msg.reactions:
        if str(reaction.emoji) == str(payload.emoji):
            if payload.member:
                return reaction, payload.member
            elif chn.guild.get_member(payload.user_id):
                return reaction, chn.guild.get_member(payload.user_id)
            return reaction, await chn.guild.fetch_member(payload.user_id)
    reaction = FalseReaction(payload.emoji, msg)
    if payload.member:
        return reaction, payload.member
    elif chn.guild.get_member(payload.user_id):
        return reaction, chn.guild.get_member(payload.user_id)
    return reaction, await chn.guild.fetch_member(payload.user_id)

async def handle_ex(ex, chn):
    await chn.send(f"`{ex}` line {ex.__traceback__.tb_lineno}")
    traceback.print_tb(ex.__traceback__)
    await chn.send(
        "Traceback:",
        file = discord.File(io.BytesIO("\n".join(traceback.format_tb(ex.__traceback__)).encode()), "tb.txt")
    )

# Updates the starboard
async def plug_starboard(msg, emojis):

    global starboard_channel_ids_by_guild_id

    stars = msg.guild.get_channel(starboard_channel_ids_by_guild_id[msg.guild.id])
    starred = []

    # Count emojis
    for reaction in msg.reactions:
        if str(reaction.emoji) in emojis:
            starred.append(str(reaction.count) + "x" + str(reaction.emoji))

    ct = msg.content
    if not ct:
        try:
            ct = msg.embeds[0].description
        except:
            pass
    if not ct:
        try:
            ct = "**" + msg.embeds[0].fields[0].name + "**\n" + msg.embeds[0].fields[0].value
        except:
            pass
    embed = embedify.embedify(
        title = 'STARBOARD ;]',
        desc = ct,
        thumb = str(msg.author.avatar_url),
        fields = [
            [
                "INFO",
                f"<@{msg.author.id}> [`{str(msg.author)}`] in <#{msg.channel.id}>\n" + \
                ', '.join(starred) + \
                f"\n[JUMP]({msg.jump_url})",
                False
            ]
        ],
        color = 0xffac34
    )
    attachments = [att.url for att in msg.attachments]
    link_attachment = False
    try:
        att = msg.embeds[0].image.url
        if att:
            attachments.append(att)
            link_attachment = True
    except:
        pass
    if " " not in msg.content and re.search(r"^https?://.*\.(png|jpg|gif|jpeg|bmp|tiff)(\?.*)?$", msg.content.lower()):
        attachments.append(msg.content)
        link_attachment = True
    if len(attachments):
        embed.add_field(
            name = 'ATTACHMENTS',
            value = ' | '.join(f"[[{i+1}]]({u})" for i, u in enumerate(attachments)),
            inline = False
        )
        try:
            if not link_attachment:
                msg.attachments[0].height
            embed.set_image(url = attachments[0])
        except:
            pass

    # Either post a new star or update the existing one
    stars_id = dbman.get('starboard', 'starboard_id', message_id = msg.id)
    try:
        if not stars_id:
            raise TypeError("stars_id isn't available yet")
        stars_msg = await stars.fetch_message(stars_id)
        delta = (stars_msg.created_at - msg.created_at).total_seconds()
        if delta <= 300:
            mins = int(delta/60)
            delta %= 60
            embed.add_field(name = "Starred in", value = f"{mins}m {delta:.0f}s")
        await stars_msg.edit(embed = embed)
    except:
        delta = (datetime.datetime.utcnow() - msg.created_at).total_seconds()
        if delta <= 300:
            mins = int(delta/60)
            delta %= 60
            embed.add_field(name = "Starred in", value = f"{mins}m {delta:.0f}s")
        stars_msg = await stars.send(embed = embed)
        dbman.insert('starboard', starboard_id = stars_msg.id, message_id = msg.id)

# Reaction add only...
async def handle_reaction_add(reaction, user):
    if (datetime.datetime.utcnow() - reaction.message.created_at).total_seconds() >= 7776000: # 3 months in seconds
        return
    if reaction.message.channel.id == starboard_channel_ids_by_guild_id[reaction.message.guild.id]: # Starred the starboard
        return

    global emojis, count, allow_multi_reactions_per_user
    chn = reaction.message.channel
    try:
        if user.id == bot.user.id:
            return
        msg = reaction.message
        mng = False

        if str(reaction.emoji) in emojis and not mng:
            # Check if the user is starring their own message
            if user.id == msg.author.id:
                await chn.send(
                    f"<@{user.id}> Don't star your own messages",
                    delete_after = 5.0
                )
                await reaction.remove(user)
            else:
                # Count all stars
                cc = 0
                react_users = [user]
                for r in msg.reactions:
                    if str(r.emoji) in emojis:
                        cc += r.count
                        if not allow_multi_reactions_per_user:
                            async for u in r.users():
                                if u.id in react_users:
                                    await chn.send(
                                        f"<@{user.id}> Don't star the same message with different stars",
                                        delete_after = 5.0
                                    )
                                    return await reaction.remove(user)
                                react_users.append(u.id)
                if count <= cc and count > 0:
                    await plug_starboard(msg, emojis)
    except IndexError:
        pass
    except Exception as ex:
        await handle_ex(ex, chn)


@bot.listen()
async def on_raw_reaction_add(payload):
    reaction, user = await from_rct_payload(payload)
    return await handle_reaction_add(reaction, user)

@bot.listen()
async def on_raw_message_delete(payload):
    # Clean out the starboard database in case it gets unstarred
    stars_id = dbman.get('starboard', 'starboard_id', message_id = payload.message_id)
    if stars_id:
        dbman.remove('starboard', starboard_id = stars_id, message_id = payload.message_id)
        return
    msg_id = dbman.get('starboard', 'message_id', starboard_id = payload.message_id)
    if msg_id:
        dbman.remove('starboard', starboard_id = payload.message_id, message_id = msg_id)
        return

@bot.listen()
async def on_ready():
    # Neat status thing
    await bot.change_presence(
        activity = discord.Activity(
            type = 3,
            name = f"the stars fly by 0.0"
        ),
        status = discord.Status.idle
    )

bot.run(TOKEN)
